// let timeStart;

addEventListener('fetch', event => {
  event.respondWith(fetchAndLog(event));
})

async function fetchAndLog(event) {
  // timeStart = Date.now();
  // console.time('delta');
  const newRequest = event.request.clone();
  const response = await fetch(event.request);

  // Forward the request to the logging system only for block events, which has a 403 response status code 3
  if (response.status = 403) {
    event.waitUntil(log(newRequest, response));
  }
  return response;
}

async function log(request, response) {

  let ray = request.headers.get('Cf-Ray') || '';
  let id = ray.slice(0, -4);
  let requestHeaders = {};
  for (var [header_k, header_v] of [...request.headers]) {
    requestHeaders[header_k] = header_v;
  }

  const request_body = await request.text()
  console.log(request_body)

  let data = {
    'timestamp': Date.now(),
    'url': request.url,
    'referer': request.referrer,
    'method': request.method,
    'ray': id,
    'status': response.status,
    'request_cf': request.cf,
    'request_body': request_body,
    'request_headers': requestHeaders

  };

  // console.timeEnd('delta')
  // data['timeDelta'] = Date.now() - timeStart;

  // console.log(data['timeDelta']);

  const url = 'http://logs-01.loggly.com/inputs/c8f1158b-4dff-4521-886f-d666182187cd/tag/http/'

  await fetch(url, {
    method: 'POST',
    body: JSON.stringify(data),
    headers: new Headers({
      'Content-Type': 'application/json',
    })
  })
}