/*
Intialize the KV with the following API call.
```
curl -X PUT \
  https://api.cloudflare.com/client/v4/accounts/$ACCOUNT_ID/storage/kv/namespaces/ab14ba5803384805bbb60a87b01ac5a9/values/counter \
  -H 'Content-Type: application/json' \
  -H "X-Auth-Email: $CLOUDFLARE_AUTH_EMAIL" \
  -H "X-Auth-Key: $CLOUDFLARE_AUTH_KEY" \
  -d '{"value": "0"}'
```
See the counter increments
```
for i in {1..5}; do curl -sv  https://httpbin.chriswang.me/anything/counter; done 
```
*/

addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request))
})

async function handleRequest(request) {
  let counter = await COUNTER_KV.get('counter', 'json')
  if (counter === null){
    return new Response('Value not found', {status: 404})
  }
  else{    
    counter['value'] = (parseInt(counter["value"])+1).toString()
    await COUNTER_KV.put('counter', JSON.stringify(counter))     //Put the json object as string
    return new Response(JSON.stringify(counter), {status: 200})
  }
}  