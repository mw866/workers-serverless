/*
Get an "A" from Scott Helme's Security Headers report: https://securityheaders.com/
THIS SOFTWARE IS PROVIDED BY CLOUDFLARE “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL CLOUDFLARE BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request))
})

async function handleRequest(request) {
  const response = await fetch(request)
  let newHeaders = new Headers(response.headers)
  newHeaders.append('x-frame-options', 'SAMEORIGIN')
  newHeaders.append('strict-transport-security', 'max-age=5184000; includeSubDomains')
  newHeaders.append('x-xss-protection', '1; mode=block')
  newHeaders.append('referrer-policy', 'no-referrer-when-downgrade')
  // newHeaders.append('content-security-policy-report-only', "default-src 'self'; report-uri https://chriswangtech.report-uri.com/r/d/csp/reportOnly")
  newHeaders.append('public-key-pins', 'pin-sha256="SQwnUDqKjiRY8k5mA5csp54tK0rVToq0hsBxurQxO4s="; pin-sha256="eU049WzcNZMd5gTYMaL00NhblpYNWwcdJrhCvv1jlb8="; max-age=5184000; includeSubDomains')

  return new Response(response.body, {
    status: response.status,
    statusText: response.statusText,
    headers: newHeaders
  })
}
