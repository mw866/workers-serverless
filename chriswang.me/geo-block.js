/*
THIS SOFTWARE IS PROVIDED BY CLOUDFLARE “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL CLOUDFLARE BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
范例代码，仅展示用途。 
*/

addEventListener('fetch', event => {
    event.respondWith(fetchAndApply(event.request))
});

async function fetchAndApply(request) {
    const blockedCities = ['Wuhan' , 'Chengdu', 'Hangzhou', 'Tianjin', 'Singapore'];
    const city = request.cf.city;
    if (blockedCities.includes(city) ) {
        return new Response('You are blocked', {
            status: 403, 
            statusText: 'Accedd Denied'
        })
    } else {
        return fetch(request)
    } 
}