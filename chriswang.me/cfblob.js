addEventListener('fetch', event => {
    event.respondWith(handleRequest(event.request))
  })
  
  async function handleRequest(request) {
    const response = new Response(
      JSON.stringify(request.cf, null, '\t'), 
      {headers: {'Content-Type': 'text/json'}}
      )
    return response
  }

  // request.cf.isEUCountry,
  // request.cf.continent,
  // request.cf.city,
  // request.cf.postalCode,
  // request.cf.latitude,
  // request.cf.longitude,
  // request.cf.timezone