/**
 * Repo: https://bitbucket.cfdata.org/projects/EA/repos/heimtor/browse/scripts/revoke_cert.js
 * Commit: fd9024a7345
 * 
 * This worker script will handle loading a CRL and doing a cert revocation check
 *
 * There are a few setup steps needed to use this script
 *  1. Copy and paste this script into a worker, and then set it on all the routes you plan on using certificate auth to access.
 *  2. Replace the CRL_URL variable below with the location of your CRL.
 *  3. Create a Cloudflare Workers KV namespace and bind it to the worker with the variable being `crlNS`
 *
 * You can force a refresh of the CRL by adding a `force-crl-refresh: 1` header to the original request
 * 
 */


addEventListener('fetch', event => {
  event.respondWith(handleRequest(event));
});

// The URL where your CRL is located. We will fetch it from here.
const CRL_URL = 'https://s3-ap-southeast-1.amazonaws.com/ca.chriswang.me/client.crl';

// The key we store your crl under in your namespace
const CRL_KV_KEY = `CRL_${btoa(CRL_URL)}`;

// Optional header that will force the worker to get an updated CRL
const FORCE_CRL_REFRESH_HEADER = 'force-crl-refresh';

/**
 * Return a 403 response
 */
function forbidden() {
  return new Response(null, { status: 403 });
}

/**
 * Fetchs a CRL list, parses out the serial numbers, and stores them into workers kv
 */
async function updateCRL() {
  const crlResp = await fetch(
    `https://api.cloudflareaccess.org/CRLHandler?url=${CRL_URL}`
  );
  if (crlResp.status == 200) {
    const newCRL = await crlResp.json();
    await crlNS.put(CRL_KV_KEY, JSON.stringify(newCRL));
    return newCRL;
  }
  return null;
}

/**
 * Load a CRL from workers kv. Handles refreshing the crl as needed.
 */
async function loadCRL(event, forceCRLRefresh = false) {
  // Force a refresh of the CRL list if needed
  if (forceCRLRefresh) {
    return await updateCRL();
  }

  let crl = await crlNS.get(CRL_KV_KEY, 'json');
  if (!crl) {
    crl = await updateCRL();
  }

  // Check to see if we should refresh the CRL
  const nextUpdate = Date.parse(crl.next_update);
  const now = new Date();
  if (now > nextUpdate) {
    event.waitUntil(updateCRL());
  }

  return crl;
}

async function handleRequest(event) {
  const request = event.request;

  // Ensure the request has the Cloudflare cf object and certificate headers and that the certificate was successfully presented
  // If so, then check the CRL to see if the cert was revoked.
  if (
    request.cf &&
    request.cf.tlsClientAuth &&
    request.cf.tlsClientAuth.certPresented &&
    request.cf.tlsClientAuth.certVerified === 'SUCCESS'
  ) {
    // Check to see if we were asked to force a CRL refresh
    const forceCRLRefresh = request.headers.get(FORCE_CRL_REFRESH_HEADER)
      ? true
      : false;

    // Load the crl
    const crl = await loadCRL(event, forceCRLRefresh);
    if (!crl) {
      return new Response('failed to load CRL', { status: 500 });
    }

    // Check to see if the certificate the user presented is in the crl
    if (crl.revoked_serial_numbers[request.cf.tlsClientAuth.certSerial]) {
      return forbidden();
    }
  }
  return await fetch(request);
}
