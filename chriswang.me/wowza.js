// THIS SOFTWARE IS PROVIDED BY CLOUDFLARE “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL CLOUDFLARE BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

addEventListener('fetch', event => {
    event.passThroughOnException()
    event.respondWith(handleRequest(event.request))
})


async function handleRequest(request) {
    let url = new URL(request.url)
    if (url.pathname.match(/.*.m3u8/)) {
        const newResponse = await fetch(request, { cf: {cacheTtl: 5}})
        return newResponse
    }
    else if (url.pathname.match(/.*.ts/)) {
        let regex = /^(.*)(\/media|\/chunklist)_.*?_.*?(.*)$/
        if (regex.exec(url.pathname)) {
            newPath = regex.exec(url.pathname)
            cacheKey = url.protocol + url.host + newPath[1] + newPath[2] + "_" + newPath[3]
            console.log("cacheKey : " + cacheKey)
            const newResponse = await fetch(request, { cf: {cacheTtl: 50, cacheKey : cacheKey}})
            const finalResponse = new Response(newResponse.body, newResponse)
            finalResponse.headers.set('cf-cacheKey', cacheKey)
            return finalResponse
        }
        const newResponse = await fetch(request, { cf: {cacheTtl: 50}})
        return newResponse
    }
    const response = await fetch(request)
    return response
}