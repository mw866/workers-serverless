addEventListener('fetch', event => {
    event.respondWith(handleRequest(event.request))
})

async function handleRequest(request) {
    start = Date.now()
    const value = await test_namespace.get("counter")
    delta = Date.now() - start
    let init = {
        headers: {
            'time': delta
        }
    };
    
    if (value === null)
    return new Response("Value not found", init)
    
    return new Response(JSON.stringify({"value": value, "time":delta}), init)
}
