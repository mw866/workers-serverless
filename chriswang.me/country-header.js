addEventListener('fetch', event => {
    event.respondWith(handleRequest(event.request))
})

async function handleRequest(request) {
    const newHeaders = new Headers(request.headers)
    newHeaders.append("X-Akamai-Edgescape", "country_code=" + request.headers.get('cf-ipcountry') )
    return fetch(request.url, {headers: newHeaders} )
}