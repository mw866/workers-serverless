/* 
THIS SOFTWARE IS PROVIDED BY CLOUDFLARE “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL CLOUDFLARE BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
Test URL /image/png?width=720
Image resizing: https://developers.cloudflare.com/images/
*/

addEventListener("fetch", event => {
  // if this request is coming from image resizing worker,
  // avoid causing an infinite loop by resizing it again:
  // if this request is coming from image resizing worker,
  // avoid causing an infinite loop by resizing it again:
  if (/image-resizing/.test(event.request.headers.get("via"))) {
    return fetch(event.request);
  }
  // now you can safely use image resizing here
  event.respondWith(handleRequest(event.request));
});

async function handleRequest(request) {
  let options = { cf: { image: { fit: "contain" } } };
  let imageURL = new URL(request.url);
  let params = imageURL.searchParams;

  const imageParams = ["width", "height", "quality"];
  for (let paramName of imageParams) {
    if (params.has(paramName)) {
      options.cf.image[paramName] = params.get(paramName);
      params.delete(paramName);
    }
  }


  options.cf.image.draw = [
    {
      url: "https://blog.cloudflare.com/content/images/2019/05/image3-1.png", // draw this image
      bottom: 5, // 5 pixels from the bottom edge
      right: 5, // 5 pixels from the right edge
      fit: "contain", // make it fit within 100x50 area
      width: 100,
      height: 50,
      repeat: "x", // repeat along the Y axis
      opacity: 0.8 // 20% transparent
    }
  ];

  console.log(JSON.stringify(options));
  console.log(imageURL.href);

  // Build a request that passes through request headers,
  // so that automatic format negotiation can work.
  const imageRequest = new Request(imageURL, {
    headers: request.headers
  });

  // Returning fetch() with resizing options will pass through response with the resized image.
  return fetch(imageRequest, options);
}
