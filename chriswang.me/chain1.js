addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request))
})


async function handleRequest(request) {
  let subrequestHeaders = new Headers();
  subrequestHeaders.set('user-agent', 'cf-worker');
  
  let subRequest = new Request('https://httpbin.chriswang.me/anything/chain2', { headers: subrequestHeaders})
  
  const response = await fetch(subRequest)
  let response_json = await response.json()
  response_json["chain1"]="cloudflare"
  return new Response(JSON.stringify(response_json))
}
