addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request))
})

async function handleRequest(request) {
  const subrequestResponse = await fetch("http://httpbin.org/flasgger_static/swagger-ui.css", {cf: {cacheTtl: 60}})
  let finalResponseHdrs = new Headers(subrequestResponse.headers)
  finalResponseHdrs.append('CF-Subrequest-Cache-Status', subrequestResponse.headers.get('CF-Cache-Status'))
  finalResponseHdrs.append('CF-Subrequest-CF-Ray', subrequestResponse.headers.get('CF-Ray'))
  finalResponseHdrs.append('CF-Subrequest-Age', subrequestResponse.headers.get('CF-Age'))


  return new Response(subrequestResponse.body, {status: subrequestResponse.status, headers: finalResponseHdrs})
}