/*
THIS SOFTWARE IS PROVIDED BY CLOUDFLARE “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL CLOUDFLARE BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*
The script does the following.
* extracts the nodeid query parameter from the request URL from the visitor.  
* based on the nodeid, looks up for the corresponding origin 
* sends the sub-request to the chosen origin
* relays the response to the visitor with a "x-origin" debug response header

Generate the Test URL from QR code image: 
$ zbarimg qrcode.png

Test commands:
$ serverless invoke -f qr-lb --querystring=transactionid=SP-QRCODE-125ba740-4391-4dee-b8fa-da95246118c6-0&nodeid=SN-1
OR 
$ curl -sv https://httpbin.chriswang.me/anything/qrlogin/?transactionid=SP-QRCODE-125ba740-4391-4dee-b8fa-da95246118c6-0&nodeid=SN-1
$ curl -sv https://httpbin.chriswang.me/anything/qrlogin/?transactionid=SP-QRCODE-125ba740-4391-4dee-b8fa-da95246118c6-0&nodeid=SN-2
*/

const originMap = new Map([
  ['SN-1', 'httpbin-sg.chriswang.me'],
  ['SN-2', 'httpbin-us.chriswang.me']
]);

const fallbackOrigin='httpbin-sg.chriswang.me';


addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request));
})


async function handleRequest(request) {
  const requestURL = new URL(request.url);
  let params = requestURL.searchParams;
  let origin = fallbackOrigin;
  
  if (params.has('nodeid')) {
    const nodeid=params.get('nodeid')
    if (originMap.get(nodeid)){
      origin=originMap.get(nodeid);
    }
   } 
  const response = await fetch(request, { cf: { resolveOverride: origin } });
  let newHeaders = new Headers(response.headers);
  newHeaders.append('x-origin', origin);
  
  return new Response(response.body, {
      status: response.status,
      statusText: response.statusText,
      headers: newHeaders
  });
}