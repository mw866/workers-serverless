# README

## Usage

### Load API credentials

```
source ~/auth.sh
```


## `weather` bog for [Twistapp](https://twist.com/index.html) using Cloudflare Workers & serverless.com

Working:
```
curl -X POST -d @data.json https://chriswang.tech/api/weathersg
```

Not working:
```
serverless  deploy --function weathersg
serverless invoke --function weathersg --path ./data.json 
```

## 'Geo'

```
serverless  deploy --function geo
serverless invoke --function geo
```
## Prettier and ESlint

### Install [Prettier](https://prettier.io/)

```
npm install --save-dev eslint-config-prettier
npx prettier --check template.js
```

## [Prettier-VSCode Integration](https://prettier.io/docs/en/editors.html#visual-studio-code)

### [Use Prettier with ESlint](https://prettier.io/docs/en/integrating-with-linters.html)

In `.eslintrc.json`:
```
{
  "extends": ["prettier"]
}
```

## References

[Cloudflare Developers: Serverless Framework](https://developers.cloudflare.com/workers/deploying-workers/serverless/)
