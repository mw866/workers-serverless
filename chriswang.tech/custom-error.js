addEventListener('fetch', event => {
    event.respondWith(fetchAndApply(event.request))
})

async function fetchAndApply(request) {  
    
    const response = await fetch(request)
    const newHeaders = new Headers(response.headers)
    newHeaders.set("Content-Type", "application/json")

    let responseBody =  {
        "success": 0,   // <--- means error response
        "server_time":  Math.round((new Date()).getTime() / 1000), //Unix ts in seconds: e.g. 1542700500,
        "error_code": 0,    // <---- the corresponding error code
        "error_message": ''  // <---- the corresponding error message
    }
    
    if (response.status == '404') {
        responseBody.error_code = 404
        responseBody.error_message = 'Not Found'
        return new Response(JSON.stringify(responseBody), {
                status: 404, 
                statusText: 'Not Found', 
                headers: newHeaders
            })
    } else {
        return response
    }
}