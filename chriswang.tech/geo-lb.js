addEventListener('fetch', event => {
  
  // Extract the client Country code from CF-IP-Country Request Header (https://support.cloudflare.com/hc/en-us/articles/200168236-What-does-Cloudflare-IP-Geolocation-do-)  
  let country = event.request.headers.get('CF-IpCountry');
  let primary_origin;
  let backup_origin;
  if (country === 'SG')  {
    // For requests from the country, resolve-override to the origin below
    primary_origin = 'nginx-lb-sg.chriswang.tech';
    backup_origin = 'nginx-lb-us.chriswang.tech';
  } else {
    //For requests from rest of world, resolve-override to the other origin
    primary_origin = 'nginx-lb-us.chriswang.tech';
    backup_origin = 'nginx-lb-sg.chriswang.tech';
  }
  
  // // Fallback if there is no response within timeout
  let timeoutId = setTimeout(function() {
    event.respondWith(fetch(event.request, { cf: { resolveOverride: backup_origin } }));
  }, 2000 /* 2 seconds */);
  
  fetch(event.request, { cf: { resolveOverride: primary_origin } })
  .then(function(response) {
    clearTimeout(timeoutId);
    event.respondWith(response);
  });  
})