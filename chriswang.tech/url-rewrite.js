/*
Re-write URL from http://api-test.chriswang.tech/httpbin/* to http://httpbin.org/$1
*/

addEventListener('fetch', event => {
    event.respondWith(handleRequest(event.request))
  })
  

async function handleRequest(request) {
    let url = new URL(request.url)
    let original_pathname = url.pathname
    url.pathname = original_pathname.replace('/httpbin','')
    url.hostname = 'httpbin.org'
    url.protocol = 'http'
    return fetch(url, request)
}