/*
Test commands:
curl -sv -X GET  http://httpbin-test.chriswang.tech/image/png
< CF-Cache-Status: MISS 
curl -sv -X GET  http://httpbin-test.chriswang.tech/image/png
< CF-Cache-Status: HIT
curl -sv -X DELETE  http://httpbin-test.chriswang.tech/image/png
{"status":"cache HIT; cache purged successfully"}
curl -sv -X DELETE  http://httpbin-test.chriswang.tech/image/png
{"status":"cache MISS; did nothing"}
*/ 

self.addEventListener('fetch', event => event.respondWith(handle(event)))

async function handle(event) {    
    let cache = caches.default    
    if (event.request.method === 'DELETE'){    
        let responseBody
        // inCache: True means the response was cached and has now been deleted; false means the response was not in the cache at the time of deletion.
        let inCache = await cache.delete(event.request, {'ignoreMethod': true})
        if (inCache){
            responseBody = {"status": "cache HIT; cache purged successfully" }
        } else {
            responseBody = {"status": "cache MISS; did nothing" }
        }
        return new Response(JSON.stringify(responseBody),  { status: 200, statusText: 'OK' })
    } else {
        return fetch(event.request)
    }
}