addEventListener('fetch', event => {
    event.respondWith(fetchAndApply(event.request))
});

async function fetchAndApply(request) {

    let redirectMap = new Map([
        // ["IN", "https://www.drbatras.com/"],
        ["AE", "https://www.drbatras.ae/"],
        ["GB", "http://www.drbatras.co.uk/"],
        ["BD", "https://www.drbatras.com.bd/"]
      ])
   
    let country = request.headers.get('CF-IpCountry')
    if (country && redirectMap.get(country)) {
        return Response.redirect(redirectMap.get(country), 302)
    } else if (country && country=="IN") {
        return fetch(request)
    }  else {
        return Response.redirect("https://www.drbatras.com/campaigns/organic/web/webdoctor.aspx", 302)
    }
}