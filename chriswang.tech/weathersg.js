// Adapted from: https://wiki.cfops.it/display/~garret/Google+Chat+Bot+-+Cloudflare+Worker#GoogleChatBot-CloudflareWorker-ConfiguringtheBot

//global static config
var config = {
  token: "653_2e8a247f46989a569086af34"
}
 
//listen for bot requests and respond accordingly
addEventListener('fetch', event => { event.respondWith(process(event.request)) })
 
//process the user request and respond
async function process(request)
{
  //create default response
  let response = {
    success: false
  }
 
  //create default init variables
  let init = {
    headers: {
      'Content-Type': 'application/json'
    },
    status: 400
  }
 
  if(request.method === 'POST')
  {
    //open request payload as json
    await request.json().then(async data =>
    {
      //validate request token as authentic
      if(config.token == data['verify_token'])
      {
        //if request type is message
        if(data['event_type'] == 'message')
        {
          response.text = echo(data)
          init.status = 200
          response.success = true
        }
      }
      else if(data['event_type'] == 'ping'){
          response.text = {"content": "pong"}
          init.status = 200
          response.success = true
      }
      else
      {
        //authentication failed
        init.status = 403
        response.error = "Unauthorized"
      }
    })
  }
  else //method not allowed
    init.status = 405
   
  //return the provided JSON response and initialization parameters
  return new Response(JSON.stringify(response), init)
}
 
//echo a google chat request message back
function echo(input)
{
  return 'You just said: ' + String(input['content']).trim()         
}