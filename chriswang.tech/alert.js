addEventListener('fetch', event => {
    event.respondWith(fetchAndApply(event.request))
})
  

async function fetchAndApply(request) {
     let response = fetch(request)

     fetch('https://maker.ifttt.com/trigger/cf_workers/with/key/c7gHuTfasiLb5llWtL6bt2', {
        method: 'POST',
        // headers: {'Authorization': 'XXXXXX'}
        body: JSON.stringify({"value1": response.statusText, "value2": "value2", "value3": "value3"})
      })

      return response;
}